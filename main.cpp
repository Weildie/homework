#include <iostream>
#include "src/incs.h"

using namespace std;


int main() {
    int i, j;
    cin >> i >> j;
    vector<vector<int>> matrix = InitVector(i, j);

    if (GetSum(matrix) == 0) {
        TotalSort(matrix);
    }

    return 0;
}