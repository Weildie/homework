#include <iostream>
#include <vector>
#include <stdlib.h>
#include <algorithm>
#include "incs.h"

using namespace std;

vector<vector<int>> InitVector(const int& i, const int& j) {
    vector<vector<int>> res(i, vector<int>(j, 0));
    for (int n = 0; n < i; n++) {
        for (int m = 0; m < j; m++) {
            int temp;
            cin >> temp;
            res[n][m] = temp;
        }
    }
    return res;
}

void PrintVec(const vector<vector<int>>& vec) {
    for (auto i : vec) {
        for (auto j: i) {
            cout << j << " ";
        }
        cout << endl;
    }
}

int GetSum(const vector<vector<int>>& vec) {
    int min_val = INT_MAX, max_val = INT_MIN;
    for (auto i : vec) {
        for (auto j : i) {
            min_val = min(j, min_val);
            max_val = max(j, max_val);
        }
    }
    return max_val + min_val;
}

bool Condition(const vector<int>& row) {
    for (int element : row) {
        if (element == 8) {
            return true;
        }
    }
    return false;
}

void TotalSort(const vector<vector<int>> vec) {
    for (vector<int> i : vec) {
        if (Condition(i)) {
            sort(begin(i), end(i));
        }
        for (auto j: i) {
            cout << j << " ";
        }
        cout << endl;
    }
}