#pragma once
#include <vector>

using namespace std;

// gives vector back
vector<vector<int>> InitVector(const int& i, const int& j);

// print vector
void PrintVec(const vector<vector<int>>& vec);

// gives sum of lowes and highest
int GetSum(const vector<vector<int>>& vec);

// show total (do not sort)
void TotalSort(const vector<vector<int>> vec);

// condition function (if 8 in row)
bool Condition(const vector<int>& row);